/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        var isOnline = navigator.onLine;

        if(isOnline==true){

            onOnline();
        }else{
            onOffline();
        }
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);

    },

    onDeviceReady: function() {

    },

    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);


        console.log('Received Event: ' + id);
    }


};

function onOffline() {



    $('.online').removeClass('hideclass');
    var popupdata = localStorage.getItem('role');

    if(popupdata=='member'){
        $('.offline').addClass('hideclass');
        $('.online').addClass('hideclass');
    appdata.random();
    appdata.offlineData();
    $('.overall').removeClass('hideclass');
    $('.loginicon').addClass('hideclass');
    }else if(popupdata=='admin'){
        $('.offline').addClass('hideclass');
        $('.online').addClass('hideclass');
        appdata.random();
        appdata.offlineData();
        $('.overall').removeClass('hideclass');
        $('.loginicon').addClass('hideclass');
    }
    else{
        $('.hover_bkgr_fricc').show();
        $('.online').addClass('hideclass');
        $('.offline').removeClass('hideclass');
    }
 }

function onOnline() {

    var popupdata = localStorage.getItem('role');

    if(popupdata=='member'){
        $('.offline').addClass('hideclass');
        $('.online').addClass('hideclass');
    appdata.random();
    appdata.showmessages('.innerx-home');
    appdata.totaldata('.innerx-home');
    $('.overall').removeClass('hideclass');
    $('.loginicon').addClass('hideclass');
    }else if(popupdata=='admin'){
        $('.offline').addClass('hideclass');
        $('.online').addClass('hideclass');
        appdata.random();
        appdata.showmessages('.innerx-home');
        appdata.totaldata('.innerx-home');
        $('.overall').removeClass('hideclass');
        $('.loginicon').removeClass('hideclass');
    }
    else{
        $('.hover_bkgr_fricc').show();
        $('.offline').addClass('hideclass');
        $('.online').removeClass('hideclass');
    }




}

document.addEventListener("offline", onOffline, false);
document.addEventListener("online", onOnline, false);

document.addEventListener('deviceready', function () {
    // Enable to debug issues.
    // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

    var notificationOpenedCallback = function(jsonData) {
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };

    window.plugins.OneSignal
      .startInit("bbca91bc-260a-42a2-a397-573307c6a103")
      .handleNotificationOpened(notificationOpenedCallback)
      .endInit();
  }, false);






