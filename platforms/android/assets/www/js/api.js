api=new Object();

api.root=appSettings.APIUrl;

api.key=appSettings.API_KEY;

 function Errorlog(url,postdata,error){
  console.log(url);
  console.log(postdata);
  console.log(error);
  console.log(error.responseText);
 }

api.invoke = function(actionUrl,callBack){

 var callurl=api.root+actionUrl;
  $.ajax({
    headers: {
      "API_KEY": api.key,
      "Accept": "application/json", //depends on your api
      "Content-type": "application/x-www-form-urlencoded" //depends on your api
    },
    url: callurl,
    success: function (response) {
      return (response,callBack);
    },
    fail: function (error) {
      
    }
  });

}


var connect=new Object();

connect.forceOffline=false;

connect.connected=function(){
	var networkState = navigator.connection.type;
	if(networkState==Connection.NONE){
		return false;
	}else{
		if(connect.forceOffline){
			return false;
		}else{
			return true;
		}
	}

}

connect.message=function(state){
	var states = {};
	states[Connection.UNKNOWN]  = 'Unknown connection';
	states[Connection.ETHERNET] = 'Ethernet connection';
	states[Connection.WIFI]     = 'WiFi connection';
	states[Connection.CELL_2G]  = 'Cell 2G connection';
	states[Connection.CELL_3G]  = 'Cell 3G connection';
	states[Connection.CELL_4G]  = 'Cell 4G connection';
	states[Connection.CELL]     = 'Cell generic connection';
	states[Connection.NONE]     = 'No network connection';
		return states[state];
}
